# CI Top

Interactive command-line viewer for CI status. Shows live status of CI pipelines and jobs
for all configured projects.

## Features

- Interface similar to `htop`, meaning table-based interface
- See pipeline status, if pipeline is running live job status
- Basic interaction with CI, such as restarting pipelines or jobs
- Support multiple GitLab instances
- If possible, view live job output

## Building

Build using cargo:

    cargo build --release

You need a configuration file to make it work. First, in GitLab, create a personal access
token that has the `read_api` or `api` scopes. Next you create a config file at `~/.config/citop/config.toml`
containing something like this:

```toml
[gitlab.main]
url = "https://gitlab.com"
token = "<YOUR-TOKEN>"
```

## License

MIT.
