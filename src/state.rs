use anyhow::Result;
use crossterm::event::{KeyCode, KeyEvent, MouseEvent};
use crossterm::terminal::*;
use crossterm::{cursor, queue, QueueableCommand};
use crossterm::{
    cursor::{Hide, MoveTo, Show},
    execute,
    style::Print,
    terminal::{Clear, ClearType, EnterAlternateScreen, LeaveAlternateScreen},
};
use gitlab::types::Project;
use std::collections::BTreeMap;
use std::io::{stdout, Write};
use std::sync::Arc;
use tokio::sync::Mutex;
use tokio::time::{interval, Duration};

#[derive(Clone, Debug, Default)]
pub struct State {
    size: (usize, usize),
    projects: BTreeMap<String, Project>,
    window_dirty: bool,
    content_dirty: bool,
    cursor: Cursor,
    exit: bool,
}

impl State {
    pub fn new() -> Self {
        State {
            size: (80, 20),
            ..Default::default()
        }
    }

    pub fn project_update(&mut self, project: &Project) {
        self.content_dirty = true;
        self.projects.insert(project.path_with_namespace.to_string(), project.clone());
    }

    pub async fn draw(&mut self) -> Result<()> {
        if self.window_dirty {
            self.draw_window().await;
        }
        if self.content_dirty {
            self.draw_content().await;
        }
        Ok(())
    }

    pub async fn draw_window(&mut self) -> Result<()> {
        self.window_dirty = false;
        let mut stdout = stdout();
        queue!(stdout, MoveTo(0, 0), Print("hello"))?;
        queue!(stdout, MoveTo(0, self.size.1 as u16), Print("oof"))?;
        stdout.flush();
        Ok(())
    }

    pub async fn draw_content(&mut self) -> Result<()> {
        let mut stdout = stdout();
        self.content_dirty = false;
        for (i, (name, project)) in self.projects.iter().enumerate() {
            queue!(stdout, MoveTo(0, i as u16 + 1), Print(name))?;
        }
        stdout.flush();
        Ok(())
    }

    pub fn exit(&self) -> bool {
        self.exit
    }

    pub async fn event_exit(&mut self) {
        self.exit = true;
    }

    pub async fn event_resize(&mut self, width: usize, height: usize) {
        self.size = (width, height);
        self.window_dirty = true;
    }

    pub async fn event_key(&mut self, key: KeyEvent) {
        match key {
            KeyEvent { code, modifiers } if code == KeyCode::Char('q') => {
                self.event_exit().await;
            }
            _ => {}
        }
    }

    pub async fn event_mouse(&mut self, mouse: MouseEvent) {
        match mouse {
            _ => {}
        }
    }
}

#[derive(Clone, Debug, Default)]
pub struct Cursor {
    pub project: Option<ProjectCursor>,
}

#[derive(Clone, Debug)]
pub struct ProjectCursor {
    pub project: u64,
    pub pipeline: Option<PipelineCursor>,
}

#[derive(Clone, Debug)]
pub struct PipelineCursor {
    pub pipeline: u64,
    pub job: Option<JobCursor>,
}

#[derive(Clone, Debug)]
pub struct JobCursor {
    pub job: u64,
}

/// Run drawing loop
pub async fn run(state: Arc<Mutex<State>>) -> Result<()> {
    execute!(
        std::io::stdout(),
        EnterAlternateScreen,
        Clear(ClearType::All),
        Hide,
    )?;

    let mut timer = interval(Duration::from_millis(30));
    loop {
        timer.tick().await;
        let mut state = state.lock().await;
        if state.exit() {
            break;
        }
        state.draw().await;
    }

    execute!(std::io::stdout(), Show, LeaveAlternateScreen)?;

    Ok(())
}

/// Set initial size
pub async fn set_size(state: Arc<Mutex<State>>) -> Result<()> {
    let (width, height) = crossterm::terminal::size()?;
    let mut state = state.lock().await;
    state.event_resize(width.into(), height.into()).await;
    Ok(())
}

