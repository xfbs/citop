mod config;
mod state;
mod sync;
mod ui;

use anyhow::{Context, Result};
use config::Config;
use crossterm::{
    cursor::position,
    event::{DisableMouseCapture, EnableMouseCapture, Event, EventStream, KeyCode},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode},
};
use futures::stream::StreamExt;
use state::State;
use std::path::PathBuf;
use std::sync::Arc;
use structopt::StructOpt;
use tokio::fs::File;
use tokio::io::AsyncReadExt;
use tokio::sync::Mutex;
use tokio::time::{interval, Duration};

#[derive(StructOpt, Clone, Debug)]
struct Options {
    #[structopt(long, short)]
    config: Option<PathBuf>,
}

async fn events(state: Arc<Mutex<State>>) -> Result<()> {
    let mut stream = EventStream::new();
    loop {
        let event = stream.next().await;
        let mut state = state.lock().await;
        match event {
            Some(Err(e)) => state.event_exit().await,
            Some(Ok(Event::Key(e))) => state.event_key(e).await,
            Some(Ok(Event::Mouse(e))) => state.event_mouse(e).await,
            Some(Ok(Event::Resize(w, h))) => state.event_resize(w.into(), h.into()).await,
            _ => {}
        }
        if state.exit() {
            break;
        }
    }
    Ok(())
}

async fn citop(options: &Options) -> Result<()> {
    let path = options
        .config
        .clone()
        .or_else(|| {
            xdg::BaseDirectories::with_prefix("citop")
                .expect("Cannot set up XDG base directories")
                .find_config_file("config.toml")
        })
        .context("Cannot find configuration file")?;
    let mut config_file = File::open(&path).await?;
    let mut config_data = vec![];
    config_file.read_to_end(&mut config_data).await?;
    let config: Config = toml::from_slice(&config_data)?;
    let config = Arc::new(config);
    let state = Arc::new(Mutex::new(State::new()));
    tokio::spawn(sync::sync(state.clone(), config.clone()));
    enable_raw_mode()?;
    state::set_size(state.clone()).await?;
    tokio::spawn(events(state.clone()));
    state::run(state.clone()).await;
    disable_raw_mode()?;
    Ok(())
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<()> {
    let options = Options::from_args();
    match citop(&options).await {
        Ok(_) => Ok(()),
        Err(e) => {
            eprintln!("Error: {}", e);
            Ok(())
        }
    }
}
