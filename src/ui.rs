use crate::config::Config;
use crate::state::State;
use anyhow::{Context, Result};
use futures::stream::StreamExt;
use gitlab::{AsyncGitlab as Gitlab, GitlabBuilder};
use std::collections::BTreeMap;
use std::path::PathBuf;
use std::sync::Arc;
use structopt::StructOpt;
use tokio::fs::File;
use tokio::io::AsyncReadExt;
use tokio::sync::Mutex;
use tokio::time::{sleep, Duration};

const PIPELINE_DOT: char = '●';
const PIPELINE_HOLLOW: char = '○';
const PIPELINE_CIRCLED: char = '⦿';
const PIPELINE_WHITE: char = '⦾';

pub async fn run(state: Arc<Mutex<State>>) {
    sleep(Duration::from_secs(999)).await;
}
