use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;
use url::Url;
use wildmatch::WildMatch;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Config {
    pub gitlab: BTreeMap<String, GitlabConfig>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct GitlabConfig {
    pub url: Url,
    pub token: String,
    pub repos: Option<Vec<String>>,
}

impl GitlabConfig {
    pub fn repo_matches(&self, path: &str) -> bool {
        if let Some(repos) = &self.repos {
            repos
                .iter()
                .map(|s| WildMatch::new(&s))
                .any(|w| w.matches(path))
        } else {
            true
        }
    }
}
