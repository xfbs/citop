use crate::config::{Config, GitlabConfig};
use crate::state::State;
use anyhow::{Context, Result};
use futures::stream::StreamExt;
use gitlab::api::projects::{pipelines::Pipelines, Projects};
use gitlab::api::AsyncQuery;
use gitlab::api::{paged, Pagination};
use gitlab::types::{PipelineBasic, Project};
use gitlab::{AsyncGitlab as Gitlab, GitlabBuilder};
use std::collections::BTreeMap;
use std::path::PathBuf;
use std::sync::Arc;
use structopt::StructOpt;
use tokio::fs::File;
use tokio::io::AsyncReadExt;
use tokio::sync::Mutex;
use tokio::time::interval;

pub async fn sync(state: Arc<Mutex<State>>, config: Arc<Config>) {
    for (name, config) in &config.gitlab {
        let url: String = config.url.host_str().unwrap().to_string();
        let client = GitlabBuilder::new(url, &config.token)
            .build_async()
            .await
            .unwrap();
        tokio::spawn(sync_gitlab(
            state.clone(),
            Arc::new(config.clone()),
            name.clone(),
            client,
        ));
    }
}

pub async fn sync_gitlab(
    state: Arc<Mutex<State>>,
    config: Arc<GitlabConfig>,
    name: String,
    client: Gitlab,
) -> Result<()> {
    let query = Projects::builder().membership(true).build().unwrap();

    let result: Vec<Project> = paged(query, Pagination::Limit(400))
        .query_async(&client)
        .await
        .unwrap();
    let mut state_lock = state.lock().await;
    for project in result {
        if config.repo_matches(&project.path_with_namespace) {
            state_lock.project_update(&project);
            tokio::spawn(sync_gitlab_repo(
                state.clone(),
                config.clone(),
                project.clone(),
                client.clone(),
            ));
        }
    }

    Ok(())
}

pub async fn sync_gitlab_repo(
    state: Arc<Mutex<State>>,
    config: Arc<GitlabConfig>,
    project: Project,
    client: Gitlab,
) -> Result<()> {
    let query = Pipelines::builder()
        .project(project.id.value())
        .build()
        .unwrap();

    let result: Vec<PipelineBasic> = paged(query, Pagination::Limit(10))
        .query_async(&client)
        .await
        .unwrap();
    for pipeline in result {
        //println!("{:#?}", pipeline);
    }

    Ok(())
}
